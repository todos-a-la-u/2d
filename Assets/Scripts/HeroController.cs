using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    [SerializeField] public float WalkSpeed = 1.0f;
    [SerializeField] public float RunSpeed = 3.0f;
    [SerializeField] public float JumpForce = 150.0f;
    [SerializeField] public float HeroRayCastLenght = 0.6f;

    private Rigidbody2D Rigidbody2D;
    private CircleCollider2D CircleCollider2D;
    private float RayCastLenght;
    private float Horizontal;
    private bool Grounded;

    private CapsuleCollider2D Piernas;
    private CapsuleCollider2D Cabeza;

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody2D = GetComponent<Rigidbody2D>();
        CircleCollider2D = GetComponent<CircleCollider2D>();
        RayCastLenght = (CircleCollider2D.radius) + 0.1f;
        Piernas = GetComponent<CapsuleCollider2D>();
        Cabeza = GetComponent<CapsuleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Horizontal = Input.GetAxisRaw("Horizontal");

//        Debug.DrawRay(transform.position, Vector3.down * RayCastLenght, Color.red);

        Grounded = Physics2D.Raycast(transform.position, Vector3.down, RayCastLenght);

        if (Input.GetKeyDown(KeyCode.W) && Grounded)
        {
            Jump();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Plataformas":
                Debug.Log(collision.collider.tag);
                break;

            case "Enemigos":
                Debug.Log("MUERE!!!");
                break;

            default:
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Plataformas")
        {
            Debug.Log("Trigger on: " + other.GetComponent<Collider>().tag);
        }
    }

    private void FixedUpdate() {
        Rigidbody2D.velocity = new Vector2(Horizontal * RunSpeed, Rigidbody2D.velocity.y);
    }

    private void Jump() {
        Rigidbody2D.AddForce(Vector2.up * JumpForce);
    }
}